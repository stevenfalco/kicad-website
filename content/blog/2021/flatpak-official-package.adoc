+++
title = "Official FlatPak Distribution Package"
date = "2021-01-14"
draft = true
"blog/categories" = [
    "News"
]
+++

KiCad is happy to announce the promotion of the FlatPak package to
official KiCad package status.

<!--more-->

== What is FlatPak

FlatPak is a distribution-independent packaging that provides self-contained images for applications to run under Linux.  This means that required libraries are bundled directly with the application rather than being dependent on the distribution-provided version.

== Should I use the FlatPak version of KiCad?

One of the benefits to FlatPak is the ability to run a newer version of KiCad than the one provided by your distribution.  The downside is that FlatPak includes copies of the libraries needed to run KiCad and so the application size is much larger.

We are starting to provide FlatPak support in an effort to help our Linux userbase have an optimal KiCad experience.

== What does "official KiCad package" mean?

While there are many packages listed on the KiCad link:/download[Download page], only a few are officially supported.  The list of officially supported distributions is given in the link:/help/system-requirements/[System Requirements] page.  The addition of FlatPak expands the number of distributions under which we can accept bug reports and for which we can issue fixes.

Thank you to Johannes Maibaum for his diligent efforts to bring the FlatPak up to distribution quality.  We welcome him to our packaging team and look forward to working together!

