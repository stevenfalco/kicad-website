+++
title = "KiCad's 2021 End of Year Fund Drive"
date = "2021-12-02"
draft = false
"blog/categories" = [
    "News"
]
+++

Looking back on 2021, we at the KiCad Project feel a deep sense of gratitude.
What could have been a lost year to the global pandemic and its associated effects (hello, supply chain!) has instead been a year of great progress at KiCad.
This is in large measure due to our supportive and engaged community of users.

== Community Building

We were not able to gather in person again this year.  
But despite this, we've been able to have a number of thoughtful, inspiring conversations online, beginning with the https://archive.fosdem.org/2021/schedule/track/open_source_computer_aided_modeling_and_design/[2021 FOSDEM conference,window=_blank].
We met a number of additional community members at the https://2021.oshwa.org/[2021 online OSHW summit,window=_blank].

In each interaction, we hear the same request: "Please release the next KiCad sooner!"

This year, we are doing just that.  KiCad version 6 will be released this month.  
And moving forward, each KiCad version will be https://dev-docs.kicad.org/en/rules-guidelines/release-policy/#_2_major_release_policy[on a single-year development cycle,window=_blank].

This change will be difficult but we are committed to making it work.
In order for this development schedule to be a success, we will need to hire full-time staff programmers.
Your support during this fund drive forms the backbone of funding that KiCad needs to make this transition successful.
Please consider making a 
+++<a href="https://donate.kicad.org" data-bs-toggle="modal" data-bs-target="#donate-modal">monthly contribution</a>+++
and drive our ability to improve KiCad faster than ever before.

== Funding Drive 

Thanks to a generous donation from https://www.kipro-pcb.com/[KiCad Services Corporation], we are currently running a matching donation campaign through January 15th.  All donations will be matched dollar for dollar by KiCad Services.  Please consider 
+++<a href="https://donate.kicad.org" data-bs-toggle="modal" data-bs-target="#donate-modal">donating today</a>+++
and make your contribution to KiCad go further than ever. 