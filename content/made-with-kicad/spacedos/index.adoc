+++
title = "SPACEDOS dosimeters"
projectdeveloper = "Universal Scientific Technologies s.r.o. (UST)"
projecturl = "https://www.ust.cz/UST-dosimeters/SPACEDOS/"
"made-with-kicad/categories" = [ "Space" ]
+++

SPACEDOS dosimeters are link:https://github.com/UniversalScientificTechnologies/SPACEDOS01[open-source] semiconductor detector based ionizing radiation spectrometers and dosimeters. They have already successfully participated in several space missions. SPACEDOS02 measures, for a period of a regular crew change lasting several months, aboard the International Space Station (ISS). SPACEDOS01 dosimeter has been modified for mounting inside CubeSats and was sent to Earth’s orbit as a part of the SOCRAT-R satellite.
