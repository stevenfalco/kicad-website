+++
title = "Upconverter"
projectdeveloper = "Opendous Inc."
projecturl = "https://code.google.com/p/opendous/wiki/Upconverter"
"made-with-kicad/categories" = [
    "Wireless"
]
+++

MF/HF Upconverter for Receivers
